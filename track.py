#!/usr/bin/env python3
from typing import Optional
import logging
import urllib.request
import gi
gi.require_version('Geoclue', '2.0')
from gi.repository import Geoclue

class TrackClient:
    def __init__(self):
        self.location:Optional[Geoclue.GClueLocation] = None;

    def update_location(self) -> None:
        """Update the current location from GPS"""
        # https://howtotrainyourrobot.com/building-a-mobile-app-for-linux-part-4-gps-m obile-tracking/
        precision: Geoclue.AccuracyLevel = Geoclue.AccuracyLevel.EXACT
        clue = Geoclue.Simple.new_sync(desktop_id = str(t.__class__),
                                       accuracy_level =precision,
                                       cancellable = None)
        self.location = clue.get_location()

    def post_location(self) -> bool:
        """Post the last save location to phoneTrack

        Will not do anything if no location has been posted yet.
        :returns: True on success, False on Failure."""
        if self.location is None:
            return False
        lat = self.location.get_property('latitude')
        lon = self.location.get_property('longitude')
        #timestamp: GLib.Variant(tt) [sec, usec]
        timestamp = self.location.get_property('timestamp')[0]

        URL="{}/apps/phonetrack/logGet/{}/{}?lat={}&lon={}&timestamp={}"
        base_url = "https://cloud.sspaeth.de"
        token="7568216514f0706f35f3235d4f674542"
        name = "mobian"
        url = URL.format(base_url, token, name, lat, lon, timestamp)
        logging.debug("Logging to URI %s",url)

        urllib.request.urlopen(url)
        return True

if __name__ == '__main__':
    t = TrackClient()
    t.update_location()
    t.post_location()
