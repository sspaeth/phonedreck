phoneDreck
==========

A simple client for logging your location and sending it to a
Nextcloud phoneTrack server. So far, it is invoked with a cron script
and will post you location whenever it is invoked (and then exist).

Currently, it requires configuration by setting it in the source code.

It also works for all other servers which allow to update locations with
a simple HTTP GET or PUT.

Requirements

sudo apt install python3-gi gir1.2-geoclue-2.0

License
=======

See file LICENSE, the full text of the license body is quoted here for
your convenience:

  0. You just DO WHAT THE FUCK YOU WANT TO.
